/*
Теоретичні питання

1. Опишіть своїми словами, що таке метод об'єкту

   Методи — це дії, які можна виконувати над об’єктами. Методи зберігаються у властивостях як визначення функцій.
   Метод об'єкту - є функцією пов'язаною з об'єктом у програмуванні об'єктно-орієнтованого стилю. В об'єктно-
   орієнтованому програмуванні дані та функції, які з ними повязані, упорядковано в об'єкти. Методи є спеціальними
   функціями, які можуть бути викликані на об'єкті для виконання певних дій чи операцій над цим об'єктом.
   Іншими словами, метод об'єкту є функцією або підпрограмою, яка прив'язана до певного об'єкта або класу. Він визначає,
   як об'єкт може виконувати певну дію або операцію. Методи дозволяють об'єктам взаємодіяти один з одним і змінювати
   свій стан.

2. Який тип даних може мати значення властивості об'єкта?

   Значення властивості обєкта в JavaScript можуть бути: String, number, boolean, null, underfined, object, function.

3. Об'єкт це посилальний тип даних. Що означає це поняття?

   Об'єкт, як посилальний тип даних означає, що змінна, яка містить об'єкт містить обєкт, де зберігається цей об'єкт, а
   при передачі об'єкта в іншу змінну, або при його копіюванні передається саме посилання.
 */

function createNewUser(userFirstName, userLastName) {
    userFirstName = prompt("Ваше ім'я?");
    userLastName = prompt("Ваше прізвище?");

    return {
        _firstName: userFirstName,
        _lastName: userLastName,

        get login() {
            return (this._firstName[0] + this._lastName).toLowerCase();
        },

        get firstName() {
            return this._firstName;
        },

        set firstName(value) {
            if (isNaN(Number(value))) {
                return this._firstName = value;
            }
        },

        get lastName() {
            return this._lastName;
        },

        set lastName(value) {
            if (isNaN(Number(value))) {
                return this._lastName = value;
            }
        }
    };
}

const newUser = createNewUser();
console.log(newUser.login);

